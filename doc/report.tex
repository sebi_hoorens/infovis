\documentclass[11pt,a4paper]{article}
\usepackage{a4wide}

\usepackage[english]{babel}

\usepackage{microtype}


%\usepackage[scaled=.8]{beramono}
\usepackage{lmodern}
\usepackage[T1]{fontenc}

\usepackage[nott]{kpfonts}


\usepackage{amsmath}
\usepackage{mathtools}

\usepackage{enumitem}
%\usepackage[htt]{hyphenat}
\usepackage{longtable}


\usepackage{natbib}
\usepackage{cite}
%\usepackage{apacite}

\usepackage[pdftex,
            pdfauthor={S\'ebastien Hoorens, Luka Rukonic, Anas Helalouch, Mohamed Zahir},
            pdftitle={Part-of-Speech Tag Similarity},
            pdfsubject={Project Information Visualization},
            pdfkeywords={{Part-Of-Speech tagging}, {Information Visualization}, {D3}, {Natural Language Processing}}]{hyperref}
\hypersetup{colorlinks=true, linkcolor=black, urlcolor=black, citecolor=black}


\title{Project Information Visualization: \\ Part-of-Speech Tag Similarity}
\author{\href{mailto:Sebastien.Hoorens@vub.ac.be}{S\'ebastien Hoorens} \quad \hfill 
        \href{mailto:Luka.Rukonic@vub.ac.be}{Luka Rukonic} \\ 
        \href{mailto:Anas.Helalouch@vub.ac.be}{Anas Helalouch} \quad \hfill
        \href{mailto:Mohammed.Zahir@vub.ac.be}{Mohamed Zahir} }


\begin{document}


\maketitle



\section{Introduction}

\emph{Part-Of-Speech} (\emph{POS}) tags form a clustering where each word in a given sequence (a sentence) is assigned a particular class. The clustering is chosen as such that the morphology (the letters) of the tagged word and the context (the neighbouring words) are more predictable. This may seem complicated, but POS tags are a major part of  language courses and are excessively used in high-schools to study grammar. Nouns, verbs, adjectives, articles, adverbs, pronouns -- these are all examples of POS. The notion of POS originates from at least 100 BC.\,when Dionysios Thrax authored a book on Ancient Greek grammar; the first notion was presumably a few hundred years earlier. Our main interest for POS lies in its application in Natural Language Processing (NLP): \emph{Part-of-Speech tagging}. POS tagging is a computational task where the input is a sequence of words and the output should assign the correct POS tag to each word.

Our visualization serves as a tool to investigate how tags relate to each other. E.g.\,articles and adjectives seem more similar to each other (both often occur before a noun) than, say, nouns and verbs. It is also customary for corpora (specialized databases of annotated written text; our data source) to distinguish several hundreds of tags from each other by fine-graining a tag into many different tags; e.g.\,there are different tags for all the common verb conjugations, and there even are separate tags for each conjugation of \emph{to be}, \emph{to have} and \emph{to do}. Hence, the notion that some tags are more similar seems rather an obvious one. Yet, most surprisingly, there has never been, to our knowledge, any attempt whatsoever to express and visualize POS tag similarity. Tags are always treated as nominals: two tags are equal or they aren't, but there is never a distance between them (e.g.\,like numeric values). The goal of our project is to make the first attempt.


\section{Design of the Visualization}

The visualization consists of two components, both expressing similarity between tags. The simplex visualization (similar to the InfoCrystal visualization) represents distances from a three-dimensional space into a two-dimensional triangular region. The hyperbolic tree visualization represents the similarities hierarchically. We can select a family of tags in the tree and see how they are distributed in the simplex space.

\begin{figure}[h!]

\begin{center}
\includegraphics[scale=0.4]{classdiagram.png}
\caption{Classdiagram of the project, extended with the processes for generating the input data files.}
\label{cd}
\end{center}

\end{figure}

\subsection{Tree Hierarchy}

Since there are no similarity measures available yet for POS tags, we have to invent our own. In the tree hierarchy, we attempt to organize the tags hierarchically, according to how strong their grammatical agreement conform. The number, person, form and tense of verbs are example properties that express grammatical agreement. The tree is then composed as follows: each parent node specifies a feature (grammatical agreement property) and each branch corresponds with a possible value for that feature. When we follow a path down to a leave node (representing the POS tags), that path contains all grammatical agreement features and values for the tag of that leave. The similarity between two tags is expressed as the fraction of nodes that their paths have in common. The purpose of the tree visualization (opposed to the simplex) is to have our tag similarity conform as well as possible with what is considered grammatically correct.

To implement the Hyperbolic Tree we made use of the Radial Reingold Tilford Tree (Bostock 2016). We then added a number of transformations so it would have the desired functionality. Amongst the many modifications we made are: changing the space of leave nodes to the root, so they would all be equally distant, the rotation to place the selected node in the center and its children bellow it, etc.

\subsection{Simplex Space}

The tree hierarchy has some restrictions however, firstly, because POS tags do not always fit well into a tree. For example, we first considered the different pronoun tags (e.g.\,\emph{I}, \emph{you}, \emph{he}, \emph{we}\ldots) as noun subtypes, but possessive pronouns (e.g.\,\emph{my}, \emph{your}, \emph{his}, \emph{our}\ldots) also fit well among the modifier subtypes (e.g.\,adjectives, articles\ldots). Also, sometimes tags correspond with more complex logical expressions (e.g.\,``not first or third singular'') which cannot be expressed by a tree. Secondly, we need to build a new tree each time we want to express the similarities between POS tags in a different language, or from a different corpus. We therefore want to look for additional ways to express similarity between tags, and compare them with the similarities from the tree. The simplex visualization expresses similarity by first mapping all tags to three-dimensional coordinates (see appendix \ref{apx:m3}) and then projecting them in two dimensions (see appendix \ref{apx:d32}). The closer two tags are to each other in the simplex, the higher their similarity. 

The input, however, is a matrix that estimates the similarities between the row and column tags (see appendix \ref{apx:matrix}). Although there are several ways to produce such a matrix, only one simple method was explored \footnote{designing more complex algorithms is beyond the scope of this project}. But if we had multiple similarity matrices in our possession, each produced by a different method, then these methods could be compared with each other using our visualization tool. Our tool might also be helpful during the development itself of a method.

The next step is to map this matrix to a set of points (one for each tag) into a three-dimensional space. This is necessary, in the first instance, to visualize the similarities from the matrix (which we don't want to visualize directly). Secondly, in some cases, this mapping can improve the similarities expressed by the matrix (see appendix \ref{apx:m3}). The mapping is parametrized by specifying three ``root tags'' and the coordinates of a tag correspond with the similarities with each root tag (normalized, such that their sum is one).

Finally, the three-dimensional coordinates are mapped in a two-dimensional simplex \ref{apx:d32}). The simplex is a triangle where each corner corresponds with a root tag. The closer a tag point is to a root tag corner, the more similar it is to that root tag.

%Because there is some loss in information, one might argue that it would be better to use the matrix similarities directly in an application, than to use the distances between the coordinates. However, when we compare two very infrequent tags, their similarity could tend to be a bad estimate. In our case, many entries are zero (tags that were never confused with each other).
% matrix too sparse (when freq both low)

% These similarities need not be symmetric yet; we used a confusion matrix where each entry corresponds with the number of times a POS tagger tagged column tag to a word for which the tag row is the gold tag.

\subsection{Interactions}

The goal of the visualization is to allow the user to comprehend the similarity of POS tags in different ways. In order to facilitate this task, different interactions are made available to the user. When clicking a node in the Hyperbolic Tree, it is moved to the root of the tree, and its children are shown in the southern part of the tree. To mark a node two options are possible. If a whole family of tags needs to be marked to visualize their spread in the simplex, then a double-click on the parent suffices to accomplish this.  If however a single node needs to be selected, a double-click on this node will do the trick. 

The marked nodes from the Hyperbolic Tree will  be highlighted in the Simplex, allowing the user to observe the similarity between these tags. The frequency of occurrence of tags in the corpus is also integrated in the visualization by having the radius of the circles in the Simplex be determined by the frequency. In other words: the bigger the circle, the more frequent the tag associated with the circle. Because this might have the undesired effect of overloading the Simplex and possibly hiding relevant information we added two tools. The first one is a fish eye view, that allows to zoom in locations of the space and distort the surrounding area. Another  option is to filter out the less/more frequent tags by using the slider. To obtain more information about the tags or compare them, the user can click on their circle and a panel bellow the visualization will be populated with more information about the specified tag(s). Because the relevance of the Simplex highly depends on the tags that form the roots, we also added the possibility to select the 3 tags that would form the roots of the Simplex. 




\section{Results}

In order to measure the value of our work we defined two questions, relevant in the field of POS, and that our visualization would help to answer. 

\subsection{How similar are two given POS tags?}

The answer to this question can be answered in two ways with our visualization. Firstly the hierarchical structure  in the form of a Hyperbolic Tree allows us to determine the parents of each tags. To see how similar two tags are,  the number of steps needed to reach a common parent might serve as answer. Another way to find their similarity is to use the Simplex and according to the root tags selected, we can see how distant  the nodes are to each other.  This approach is more intuitive but requires a good selection of root tags.


\subsection{How much do the simplex and tree visualizations agree with each other?}

\begin{figure}[h!]

\begin{center}
\includegraphics[scale=0.22]{roots1nouns.png}
\caption{The noun family.}
\label{nn1}
\end{center}

\begin{center}
\includegraphics[scale=0.22]{roots1verbs.png}
\caption{The verb family.}
\label{vb1}
\end{center}

\begin{center}
\includegraphics[scale=0.22]{roots1modifs.png}
\caption{The modifier family.}
\label{jj1}
\end{center}

\end{figure}

We will discuss three use cases, shown in figures \ref{nn1}, \ref{vb1} and \ref{jj1}, each corresponding to a particular selection in the tree. We opted for singular nouns (NN), uninflected verb forms (VB) and adjectives (JJ) as root tags. The tags are more or less equally spread over the whole surface of the simplex, though the spread is a little smaller along the (NN-VB)-direction. The reason for this is that nouns and adjectives show some similarity (e.g.\,they often follow after a determiner or an adjective), causing the correlation in the similarities of tags with both root tags.

In figure \ref{nn1} all tags belonging to the noun family (singular/plural common/proper nouns, pronouns\ldots) are selected in the tree. If the simplex agrees with the tree hierarchy, we would expect these tags to be clustered together close to the noun root. This is indeed largely the case, with a few outliers. It is worth noticing that the two biggest outliers, NP\$ and NN\$, both correspond with genitive nouns (e.g.\,\emph{man's}, \emph{God's}, \emph{world's}\ldots), which behave more like adjectives than nouns.

In the second use case (figure \ref{vb1}), all tags from the verb family (all verb conjugations, modal verbs\ldots) are selected. Similarly as with the previous figure, we now observe that the selected tags are clustered close to the verb root. Two big outliers are VBG (``ing-form''; e.g.\,\emph{walking}, \emph{eating}, \emph{drinking}\ldots) and VBN (e.g.\,\emph{done}, \emph{burned}, \emph{given}\ldots), which are often confused with adjectives (e.g.\,``a burning sword'' or a ``burned sword'').

In the third figure, all modifiers were selected. The ``modifier'' family was made up by us to denote words that change properties of other words. Adjectives (JJ), articles (JJ) and cardinal numbers (CD) change nouns, adverbs (RB) and particles (RP) change verbs and qualifiers (e.g.\,``very'', ``half''\ldots) modify other modifiers. The selected tags are not clustered as well as in the other two cases, we we can still see that these tags tend to be close to the adjective root tag. The modifier family is much more vaguely defined than the noun and verb family, which is presumably the reason for the weak clustering we observe.

A fourth strong cluster is formed by IN, CC and CS tags which form the majority of the combiner group (words that glue sentences together; e.g.\,``because'', ``for'', ``in'', ``and''\ldots)


\appendix


\section{Work Division}

The team is composed of 4 students with different backgrounds and interests in the vast domain of Computer Science. The project to visualize similarity of POS tags was proposed by Sébastien who is currently doing a Master Thesis on POS, and after some long explanations on what the actual purpose of such visualization would be, he managed to convince the team. 

The first steps were to build the foundation for our visualization. In this phase the focus was put on defining the goals our visualization would have to reach, and what information would be shown. This phase was punctuated with the first presentation of the project.The feedback we received after the first presentation was positive so we started the design phase. It became clear after our first meeting that the visualization would have to be split in two parts, one for the selection of tags and one to explore the similarity between the selected tags. The intermediary presentation confirmed this idea, and the decision was made to use a Hyperbolic Tree for the selection and a two-dimensional Simplex for the exploration. The work was therefore divided in equal shares: Sébastien and Luka were in charge of the Simplex while Mohamed and Anas worked on the Hyperbolic Tree. 
During the implementation phase, many smaller tasks were added to the workload. So in addition to the major visualization tasks, Sébastien was in charge of the the Data-Mining and POS related content, Luka of the website structure, Anas of the Fish-Eye exploration and Mohamed added some information to the visualizations. 

Overall the load was evenly balanced between the group members and the general working atmosphere was sound. Many meetings were held, and work sessions frequently took place to make sure we were all on the same page. 


\section{Terminology}

\begin{description}

\item[Accuracy] The fraction of tokens where the tagger and the gold standard agree.

\item[Analysis] Usually used in the context of parsing (grammatical analysis), where an analysis refers to the process of assigning a parse tree (or POS tags) to an utterance. Other kinds of analyses may also be referred to (e.g.\,morphological analysis, sentiment analysis, voice analysis\ldots). An analysis may also mean a (tag) reading. Under the context of model evaluation, we may also be referring to statistical analysis.

\item[Annotation] Marking or labelling linguistic segments in the corpus itself.

\item[Corpus] A large, structured set of texts used for linguistic research. The texts are tokenized and annotated with POS tags.

\item[Gold standard tag] The tag that is to be considered the correct one from the tagset of the corpus, used during evaluation or supervised training of a tagger.

\item[Lexicon] Representation of the known words (e.g.\,stems and affixes). Often assigns a list of possible tags for a given word.

\item[Morphology] Studies the structure of words and how they are composed by morphemes.

\item[Part-of-speech (POS)] A category for words according to grammatical properties.

\item[Tag] Denotes a part-of-speech.

\item[Tagger] A model that can perform POS tagging.

\item[Tagset] A particular chosen division of POS tags. Each corpus usually specifies its own tagset.

\item[Token] Not to be confused with words, tokens are the data points that together form the input text. They are the subunits obtained after the process of tokenization.

\item[Treebank] Corpus in which each sentence is annotated with a parse tree.

\item[Utterance] Usually refers to the input format of a sentence.

\item[Word] The form that a data point (token) may take.

\item[Word form] See word.

\end{description}


\section{Tagset of the Brown Corpus}

\begin{longtable}{|p{1cm}|p{1.5cm}|p{4.5cm}|p{7cm}|}
\hline 
 \textbf{Tag} & \textbf{Count} & \textbf{Description} & \textbf{Examples} \\ 
 
\hline 
\texttt{NN} & 152\,470 & noun: singular, common & e.g.\,``fire'', ``weekend'', ``airport''\ldots \\ 
\hline 
\texttt{IN} & 120\,557 & preposition & e.g.\,``of'', ``for'', ``by'', ``to'', ``on'', ``through'', ``between'', ``without'', ``except''\ldots \\
\hline 
\texttt{AT} & 97\,959 & article & e.g.\,``the'', ``an'', ``no'', ``every''\ldots \\ 
\hline
\texttt{JJ} & 64\,028 & adjective & e.g.\,``hard'', ``widespread'', ``ambiguous'', ``recent'', ``outmoded'', ``possible''\ldots \\ 
\hline
\texttt{.} & 60\,638 & punctuation: sentence termination & e.g.\,``.'', ``?'', ``;'', ``!''\ldots \\ 
\hline
\texttt{,} & 58\,156 & punctuation: \emph{,} (comma) & \\
\hline
\texttt{NNS} & 55\,110 & noun: plural, common & e.g.\,``years'', ``laws'', ``members''\ldots \\ 
\hline
\texttt{CC} & 37\,718 & conjunction: coordinating  & e.g.\,``and'', ``but'', ``either'', ``nor'', ``yet''\ldots \\
\hline
\texttt{RB} & 36\,464 & adverb & e.g.\,``near'', ``likely'', ``often'', ``also'', ``back'', ``yet'', ``aside'', ``nevertheless'', ``upon''\ldots \\
\hline
\texttt{NP} & 34\,476 & noun: singular, proper & e.g.\,``October'', ``Jon'', ``Belgium''\ldots \\ 
\hline
\texttt{VB} & 33\,693 & verb: base form & e.g.\,``run'', ``enter'', ``investigate''\ldots \\ 
\hline
\texttt{VBN} & 29\,186 & verb: past participle  & suffix is usually -ed \\ 
\hline
\texttt{VBD} & 26\,167 & verb: past tense & suffix is usually -ed \\ 
\hline
\texttt{CS} & 22\,143 & conjunction: subordinating & e.g.\,``if'', ``because'', ``while'', ``that'', ``as'', ``once''\ldots \\
\hline
\texttt{PPS} & 18\,253 & pronoun: third person, singular, personal, nominative & e.g.\,``it'', ``he'', ``she''\ldots \\
\hline
\texttt{VBG} & 17\,893 & verb: present participle or gerund & suffix is usually -ing \\ 
\hline
\texttt{PP\$} & 16\,872 & determiner: possessive & e.g.\,``their'', ``your'', ``my'', ``mine''\ldots \\ 
\hline
\texttt{TO} & 14\,918 & \emph{to} (infinitival) & \\ 
\hline
\texttt{PPSS} & 13\,802 & pronoun: personal, nominative, not 3rd person singular  & e.g.\,``we'', ``I'', ``you'', ``thou''\ldots \\ 
\hline
\texttt{CD} & 13\,510 & numeral: cardinal & e.g.\,``one'', ``fifty-three'', ``7.5 billion'', ``3.14''\ldots \\ 
\hline
\texttt{NN-TL} & 13\,372 & noun: titles & e.g.\,``Dr.'', ``University'', ``County'', ``Union''\ldots \\ 
\hline
\texttt{MD} & 12\,431 & modal auxillary & e.g.\,``should'', ``would'', ``will'', ``can'', ``shall'', ``need''\ldots \\
\hline
\texttt{PPO} & 11\,181 & pronoun: personal, accusative & e.g.\,``them'', ``us'', ``you'', ``'em''\ldots \\ 
\hline
\texttt{BEZ} & 10\,066 & \emph{is} & \\ 
\hline
\texttt{BEDZ} & 9\,806 & \emph{was} & \\ 
\hline

\caption{25 most frequent tags from the Brown corpus tagset.}
\label{brown}
\end{longtable}


\section{Technicalities}

\subsection{Methods for Producing the Similarity Matrix} \label{apx:matrix}

The main idea is that similar tags occur with similar words and in similar contexts. If we would create a large data set with as columns features the current/next/previous word/tag, we are interested in how the value in the current tag column is affected by the values in the other columns. This is related to correlation, which inspects how the values in two columns help predict each other. However, in our case we want to look at one column with respect to all the others and there is no general method for this kind of problem. One, arguably naive, solution is to express similarity by comparing the current tag column with each other column separately and then take an averaged sum for all columns, but taking averages often leads to a bad spread.

Instead, we opted for a confusion matrix, which makes use of a POS tagger. The rows correspond with gold tags and the columns with the tags assigned by the tagger. Each entry equals the fraction of words from the corpus that have the row gold tag, and where assigned the column gold tag. Note that the produced matrix is not symmetric; we resolve this issue by taking the average of the similarities in both orders, weighted by their frequency.



\subsection{Mapping the Similarity Matrix to Three Dimensions} \label{apx:m3}

Next we map each tag to its coordinate by looking up the similarities with each root tag. We put these three values in a vector and normalize it such that the three coordinates sum to one. During this mapping, we lose all information about the similarities between two non-root tags. However, as in our case with the confusion matrix, similarities between two unfrequent tags can be very badly estimated. Re-estimating these similarities using good estimations might result in better estimations; here, the similarities with the root tags are used. Hence, if we use frequent tags as roots, our similarities may still improve, despite the information loss.

The quality of our similarities therefore strongly depends on our choice of root tags. Frequent tags make good roots, but it is also important that the similarities between the three roots is sufficiently small, otherwise this would result in a bad spread. We also found that tags with a large number of different words make better roots, probably because tags with few different words (e.g.\,articles) are seldom confused (resulting in lower counts in the confusion matrix with other tags, resulting in worse estimates).


\subsection{Mapping from Three Dimensions to Two Dimensions} \label{apx:d32}

Mapping a three-dimensional point $\mathbf{x}$ to a two-dimensional point $\mathbf{y}$ is realized through multiplication with the transformation matrix $A$:
\begin{equation}
\mathbf{y} = A \cdot \mathbf{x}
\end{equation}
Where the column vectors of the transformation correspond with the coordinates three corners (the root tags). Hence, we can choose any coordinates we want for the corners by directly putting these values in the transformation matrix. The reason that this works is that the corner coordinates happen to correspond with the unit vectors ($[1,0,0]$, $[0,1,0]$ and $[0,0,1]$), which mean 100\% similarity with one of the roots and 0\% similarity with the other roots. Specifying the mapping for the unit vectors is the only requirement to construct a transformation matrix.
\citep{christodoulopoulos2010}

More intuitively, the reason that we can eliminate one dimension without any information loss is because we now that the three coordinate values must sum to one. We only need to know two coordinates and can always find the third one by subtracting both values from one. Using a transformation matrix is very efficient and it easily allows us to change the corner coordinates.


{\footnotesize
\bibliography
{christodoulopoulos2010}
{}
}
\bibliographystyle{apalike}

%{Bostock, M.(2016). Radial Reingold?Tilford Tree. Retrieved May 12, 2016, from https://bl.ocks.org/mbostock/4063550}

\end{document}