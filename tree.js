
function mapTagTree(tree) {
    if (tree.leaf) {
        return { rule : tree.branchValue 
               , name : tree.leaf };
    }
    else {
        return { rule : tree.branchValue 
               , name : tree.childFeature 
               , children : tree.children.map(mapTagTree) };
    }
}

var root;
var maxDepth;
d3.json("tagtree.json", function(data) {
	root = mapTagTree(data);
	root.x0 = height / 2;
        root.y0 = 0;
	maxDepth = getDepth(root);
	initialTreeUpdate(root);
});					  


var diameter = Math.min(width, height);
    
var i = 0,
    duration = 350,
    root;

var tree = d3.layout.cluster()
    .size([360, diameter / 2 - 40])
    .separation(function(a, b) { return (a.parent == b.parent ? 1 : 5) / a.depth; });


var diagonal = d3.svg.diagonal.radial()
    .projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });

var treesvg = d3.select("#tree").append("svg")
    .attr("width", width + margin.left)
    .attr("height", height + margin.top)
	.append("g")
    .attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");


var idOfParent = 9999999;
var selectedNodes = [];
//Keeps the selected node's parent
var parentNode = null;





//root.children.forEach(collapse); // start with all children collapsed
// update(root);

d3.select(self.frameElement).style("height", "800px");

function initialTreeUpdate(source) {
	
	console.log("click in update", source);

  // Compute the new tree layout.
  var nodes = tree.nodes(root),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  //nodes.forEach(function(d) { d.y = d.depth * 80; });

  // Update the nodes…
  var node = treesvg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); }, function(d) { return d.isSelected || (d.isSelected = false); });

  // Enter any new nodes at the parent's previous position.
  
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      //.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })
      .on("click", click)
      .each(function(d)
      {
      	var sel = d3.select(this);
        //var state = false;
        sel.on("dblclick", function(e)
        {
			console.log("dblckick", e);
        	//window.alert("state is: " + state);
        	//state = !state;
          
          if (!e.isSelected) {
          	e.isSelected = true;
            
            //Select all nodes
              console.log("I clicked an unselected node");
			
            
            selectSubtree(e);
			updateClick(e);
			console.log("selected nodes", model.selection);
			
            
          } else {
          	e.isSelected = false;
           

            //deselect all nodes
             console.log("I clicked an already selected node");
            
            deselectSubtree(e);
			updateClick(e);
				console.log("selected nodes", selectedNodes);
            
          }
        });
      });
      


  nodeEnter.append("circle")
  .attr("id", function(d){return d.name})
      .attr("r", 1e-6)
.style("fill", function(d) { return isSelectedChild(d) ? "blue" : "white"; });
      //.style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeEnter.append("text")
      .attr("x", 10)
      .attr("dy", ".35em")
      .attr("text-anchor", "start")
      //.attr("transform", function(d) { return d.x < 180 ? "translate(0)" : "rotate(180)translate(-" + (d.name.length * 8.5)  + ")"; })
      .text(function(d) { return d.name; })
      .style("fill-opacity", 1e-6);

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })

  nodeUpdate.select("circle")
      .attr("r", 4.5)
		.style("stroke", "#779696")
      .style("fill", function(d) { return isSelectedChild(d) ? "blue" : "white"; });

  nodeUpdate.select("text")
      .style("fill-opacity", 1)
      .attr("transform", function(d) { return d.x < 180 ? "translate(0)" : "rotate(180)translate(-" + (d.name.length + 50)  + ")"; });

  // TODO: appropriate transform
  var nodeExit = node.exit().transition()
      .duration(duration)
      //.attr("transform", function(d) { return "diagonal(" + source.y + "," + source.x + ")"; })
      .remove();

  nodeExit.select("circle")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

  // Update the links…
  var link = treesvg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link");


	link .style("stroke", "grey")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      });
  
  


  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);



  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });

nodes.forEach(function (d){
d.isSelected = false;
});

}

//Update the graph when a node is clicked
// TODO : center the selected node
// TODO : add the parent as a child
function updateClick(source) {
		console.log("click in updateClick " + source);
	
	// plan: take selected node's parent, add it to the children of selected node in vizu
	var originalroot = root;
	if (source.children == null)
	return;
	var oldParent = source.parent;
	if (oldParent != null){
	var offsetX = 90- oldParent.x;
	idOfParent = clone(oldParent.id);
	}
	else{

	}

	makeRoot(source);
	root = source;
	

  // Compute the new tree layout.
  var nodes = tree.nodes(source),
//	nodesOriginal = tree.nodes(originalroot),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  //nodes.forEach(function(d) { d.y = d.depth * 80; });
  

  // Update the nodes…
  var node = treesvg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); }, function(d) { return d.isSelected || (d.isSelected = false); });

  // Enter any new nodes at the parent's previous position.
  
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      //.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })
      .on("click", click)
      .each(function(d)
      {
      	var sel = d3.select(this);
        //var state = false;
        sel.on("dblclick", function(e)
        {
        	//window.alert("state is: " + state);
        	//state = !state;
          
          if (!e.isSelected) {
          	//e.isSelected = true;
            
            //Select all nodes
          
           // selectSubtree(e);
            
          } else {
          	//e.isSelected = false;
           

            //deselect all nodes
            
            
            //deselectSubtree(e);
            
          }
        });
      });
      


  nodeEnter.append("circle")
  .attr("id", function(d){return d.name})
      .attr("r", 1e-6)
.style("fill", function(d) { return isSelectedChild(d) ? "#ACE39A" : "white"; });
      
      //.style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeEnter.append("text")
      .attr("x", 10)
      .attr("dy", ".35em")
      .attr("text-anchor", "start")
      //.attr("transform", function(d) { return d.x < 180 ? "translate(0)" : "rotate(180)translate(-" + (d.name.length * 8.5)  + ")"; })
      .text(function(d) { return d.name; })
      .style("fill-opacity", 1e-6);

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "rotate(" + (d.x + -00) + ")translate(" + d.y + ")"; })

  nodeUpdate.select("circle")
      .attr("r", 7.5)
      .style("fill", function(d) { return isSelectedChildInModel(d) ? "#ACE39A" : "white"; });

  nodeUpdate.select("text")
      .style("fill-opacity", 3)
      .attr("transform", function(d) { return d.x < 180 ? "translate(0)" : "rotate(180)translate(-" + (d.name.length + 50)  + ")"; });

  // TODO: appropriate transform
  var nodeExit = node.exit().transition()
      .duration(duration)
      //.attr("transform", function(d) { return "diagonal(" + source.y + "," + source.x + ")"; })
      .remove();

  nodeExit.select("circle")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

var clickedNodes = [];
clickedNodes.push(source);
  // Update the links…
  var link = treesvg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; })
	.attr("transform", "rotate(+90)" )
	link .style("stroke", function(d){
		console.log(d);
		if (isPartOfArray(d.source, clickedNodes))
		{
			if (d.target.id != idOfParent)
			{ clickedNodes.push(d.target);
		return "#7DBF67";
	}}
		else
		return "grey";
	})
	;



  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link")
	

      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      })
.attr("transform", "rotate(+90)" );

  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();


  // Stash the old positions for transition.
 nodes.forEach(function(d) {
    d.x0 = d.x ;
    d.y0 = d.y;
  });

updateSimplex();
}